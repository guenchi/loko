;; Copyright © 2020 Gwen Weinholt
;; SPDX-License-Identifier: EUPL-1.2+
(define FOO-B
  (begin
    (set! counter (+ counter 1))
    counter))
